package fr.mycompany.common;


import fr.mycompany.raw.m1.*;
import org.apache.avro.generic.*;
import org.junit.*;

import java.util.*;

import static org.junit.Assert.*;

public class RawRecordParserTest {

    String json(){
        return "{\"charging_source\":\"solar\",\"processor4_temp\":74,\"device_id\":\"a9138060-aeff-4ae9-99e8-562b97664409\",\"processor2_temp\":195,\"processor1_temp\":211,\"charging\":-247,\"current_capacity\":1454,\"inverter_state\":10,\"moduleL_temp\":32,\"moduleR_temp\":132,\"processor3_temp\":145,\"SoC_regulator\":26.502897}\n";
    }

    String base64Event(){
        return  "SGE5MTM4MDYwLWFlZmYtNGFlOS05OWU4LTU2MmI5NzY2NDQwOe0DCnNvbGFypgOGA9wWFECIAqICq0GY272AOkA=";
    }

    @Test
    public void shouldEncode(){
        final Optional<RawRecord> record = RawRecordParser.fromJsonAvro(json());
        assertTrue(record.isPresent());
    }

    @Test
    public void shouldBeSerializedToBase64(){
        final Optional<RawRecord> record = RawRecordParser.fromJsonAvro(json());
        assertEquals(base64Event(), RawRecordParser.toBase64(record.get()).get());
    }

}