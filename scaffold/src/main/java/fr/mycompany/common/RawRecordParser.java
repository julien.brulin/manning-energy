package fr.mycompany.common;


import fr.mycompany.raw.m1.*;
import org.apache.avro.*;

import org.slf4j.*;

import java.io.*;
import java.util.*;

public class RawRecordParser implements SpecificAvroParser<RawRecord> {

    private static Logger logger = LoggerFactory.getLogger(RawRecordParser.class.getName());

    private static Schema schema;

    static {
        try {
            schema = new Schema.Parser()
                    .parse(RawRecordParser.class.getResourceAsStream("/avro/DeviceRecord.avsc"));
        } catch (IOException ioe) {
            throw new ExceptionInInitializerError(ioe);
        }
    }

    private static Base64.Encoder base64 = Base64.getEncoder();

    public static Optional<RawRecord> fromJsonAvro(String event) {
        return new RawRecordParser().fromJsonAvro(schema, event, RawRecord.class);
    }



    public static Optional<String> toBase64(RawRecord record) {
        return new RawRecordParser().toBase64(schema, record);
    }

}
