package fr.mycompany.common;

import org.apache.avro.*;
import org.apache.avro.generic.*;
import org.apache.avro.io.*;
import org.apache.avro.specific.*;

import java.io.*;
import java.util.*;

public interface SpecificAvroParser<T> {

    static Base64.Encoder base64 = Base64.getEncoder();

    default Optional<T> fromJsonAvro(Schema schema, String event, Class<T> type) {
        try (InputStream is = new ByteArrayInputStream(event.getBytes()); DataInputStream din = new DataInputStream(is)) {
            Decoder decoder = DecoderFactory.get().jsonDecoder(schema, din);
            DatumReader<T> reader = new SpecificDatumReader<T>(schema);
            return Optional.of(reader.read(null, decoder));
        } catch (IOException | AvroTypeException e) {
            return Optional.empty();
        }
    }

    default Optional<String> toBase64(Schema schema, T record) {
        try (ByteArrayOutputStream bout = new ByteArrayOutputStream()) {
            DatumWriter<T> writer = new SpecificDatumWriter<>(schema);
            BinaryEncoder encoder = EncoderFactory.get().binaryEncoder(bout, null);
            writer.write(record, encoder);
            encoder.flush();
            return Optional.of(base64.encodeToString(bout.toByteArray()));
        } catch (IOException e) {
            System.out.println("Error serializing:" + e.getMessage());
            return Optional.empty();
        }

    }
}
