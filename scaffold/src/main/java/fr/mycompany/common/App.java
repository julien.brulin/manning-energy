package fr.mycompany.common;

import fr.mycompany.m1.api.*;
import fr.mycompany.m1.stream.*;
import io.dropwizard.*;
import io.dropwizard.jdbi3.*;
import io.dropwizard.lifecycle.*;
import io.dropwizard.setup.*;
import org.jdbi.v3.core.*;

public class App extends Application<DeviceStateStreamConf> {

    @Override
    public void initialize(Bootstrap<DeviceStateStreamConf> bootstrap) {
        bootstrap.addCommand(new DeviceStateStream(bootstrap.getApplication()));
    }


    @Override
    public void run(DeviceStateStreamConf config, Environment environment) throws Exception {

    }
}
