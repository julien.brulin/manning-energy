package fr.mycompany.m1.api;

import com.fasterxml.jackson.annotation.*;
import io.dropwizard.Configuration;
import io.dropwizard.db.*;


import javax.validation.*;
import javax.validation.constraints.*;
import java.util.*;

public class ApiConfiguration extends Configuration {

    @Valid
    @NotNull
    private DataSourceFactory dataSourceFactory = new DataSourceFactory();

    @JsonProperty("database")
    public void setDataSourceFactory(DataSourceFactory factory){
        this.dataSourceFactory = factory;
    }

    @JsonProperty("database")
    public DataSourceFactory getDataSourceFactory(){
        return this.dataSourceFactory;
    }

    @NotNull
    private final String deviceTable = "devices";

    @JsonProperty("deviceTable")
    public String getDeviceTable() {
        return deviceTable;
    }

    @JsonProperty("kafka")
    public Map<String, String> getKafka() {
        return kafka;
    }

    @NotNull
    private Map<String, String> kafka;

    @JsonProperty("kafka")
    public void setKafka(Map<String, String> kafka) {
        this.kafka = kafka;
    }

    @NotNull
    private String topic;

    @JsonProperty("topic")
    public String getTopic() {
        return topic;
    }

    @JsonProperty("topic")
    public void setTopic(String topic) {
        this.topic = topic;
    }

}
