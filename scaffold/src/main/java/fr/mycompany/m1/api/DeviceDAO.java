package fr.mycompany.m1.api;

import org.jdbi.v3.sqlobject.customizer.*;
import org.jdbi.v3.sqlobject.statement.*;

import java.util.*;

public interface DeviceDAO {

    @SqlQuery("SELECT deviceId FROM <table>")
    List<String> getDeviceIds(@Define("table") String table);

    @SqlQuery("SELECT charging FROM <table> WHERE deviceId = :deviceId")
    boolean isCharging(@Define("table") String table, @Bind("deviceId") String deviceId);

    @SqlUpdate("INSERT INTO <table> (deviceId, charging) values (:deviceId, :charging) ON CONFLICT (deviceId) DO UPDATE SET charging = excluded.charging")
    void setDeviceState(@Define("table") String table, @Bind("deviceId") String deviceId, @Bind("charging") boolean charging);
}
