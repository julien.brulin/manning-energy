package fr.mycompany.m1.api;

import io.dropwizard.lifecycle.*;
import org.apache.commons.io.*;
import org.apache.kafka.clients.producer.*;

import javax.inject.*;

public class CloseableManaged implements Managed {

    private final DatabaseManagement databaseManagement;

    private final KafkaProducer kafkaProducer;

    public CloseableManaged(DatabaseManagement databaseManagement, KafkaProducer kafkaProducer) {
        this.databaseManagement = databaseManagement;
        this.kafkaProducer = kafkaProducer;
    }

    @Override
    public void start() throws Exception {
        databaseManagement.create();

    }

    @Override
    public void stop() throws Exception {
        IOUtils.closeQuietly(kafkaProducer);
    }
}
