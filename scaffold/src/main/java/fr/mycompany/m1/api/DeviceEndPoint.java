package fr.mycompany.m1.api;


import fr.mycompany.common.*;
import org.apache.kafka.clients.producer.*;

import javax.servlet.http.*;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.*;
import java.util.*;
import java.util.stream.*;

@Path("/")
public class DeviceEndPoint {

    private final KafkaProducer kafkaProducer;

    private final RawRecordParser rawRecordParser;

    private final String topic;

    private final Callback stats;

    private final DeviceDAO deviceDAO;

    private final String deviceTable;

    public DeviceEndPoint(KafkaProducer kafkaProducer, String topic, DeviceDAO deviceDAO, String deviceTable) {
        this.kafkaProducer = kafkaProducer;
        this.deviceDAO = deviceDAO;
        this.rawRecordParser =  new RawRecordParser();
        this.topic = topic;
        this.stats = new ProducerStats().create();
        this.deviceTable = deviceTable;
    }

    @GET()
    @Path("/devices")
    @Produces(MediaType.APPLICATION_JSON)
    public List<String> deviceIds(){
        return deviceDAO.getDeviceIds(deviceTable);
    }

    @GET()
    @Path("/devices/{uuid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Boolean isCharging(@PathParam("uuid") String deviceId){
        return deviceDAO.isCharging(deviceTable, deviceId);
    }

    @POST
    @Path("/send/{uuid}")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM })
    @Produces(MediaType.APPLICATION_JSON)
    public Response send(@PathParam("uuid") String eventId, @Context HttpServletRequest request){
        try {
            List<String> jsonRawRecords = loadJsonRawRecords(request.getInputStream());
            jsonRawRecords.stream().map(s -> rawRecordParser.fromJsonAvro(s)).filter(Optional::isPresent)
                    .forEach( r -> {
                kafkaProducer.send(new ProducerRecord(topic, eventId, r.get()), stats);
            });
        } catch (IOException e) {
            throw new IllegalStateException("Failed to read request body !", e);
        }
        return Response.noContent().build();
    }

    private List<String> loadJsonRawRecords(InputStream request){
        return new BufferedReader(new InputStreamReader(request)).lines().collect(Collectors.toList());
    }

    private static class StatsRest {

        private Long errorCount;

        private long successCount;

        private StatsRest(Long errorCount, long successCount) {
            this.errorCount = errorCount;
            this.successCount = successCount;
        }

        public static StatsRest from(ProducerStats stats){
            return new StatsRest(stats.getErrorCount().get(), stats.getSuccessCount().get());
        }

        public Long getErrorCount() {
            return errorCount;
        }

        public void setErrorCount(Long errorCount) {
            this.errorCount = errorCount;
        }

        public long getSuccessCount() {
            return successCount;
        }

        public void setSuccessCount(long successCount) {
            this.successCount = successCount;
        }
    }
}
