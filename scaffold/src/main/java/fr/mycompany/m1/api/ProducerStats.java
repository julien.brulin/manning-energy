package fr.mycompany.m1.api;

import org.apache.kafka.clients.producer.*;
import org.slf4j.*;

import java.util.concurrent.atomic.*;

public class ProducerStats {

    private static final Logger log = LoggerFactory.getLogger(ProducerStats.class);

    private final AtomicLong errorCount = new AtomicLong();

    private final AtomicLong successCount = new AtomicLong();

    public Callback create(){
        return (recordMetadata, e) -> {
            if (e != null) {
                log.error("Error adding to topic", e);
                errorCount.incrementAndGet();
            } else {
                successCount.incrementAndGet();
            }
        };
    }

    public AtomicLong getErrorCount() {
        return errorCount;
    }

    public AtomicLong getSuccessCount() {
        return successCount;
    }
}
