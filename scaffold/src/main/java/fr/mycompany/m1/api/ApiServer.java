package fr.mycompany.m1.api;

import fr.mycompany.m1.stream.*;
import fr.mycompany.raw.m1.*;
import io.confluent.kafka.serializers.*;
import io.confluent.kafka.streams.serdes.avro.*;
import io.dropwizard.jdbi3.*;
import io.dropwizard.setup.*;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.*;
import org.jdbi.v3.core.*;

import java.util.*;

import static io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG;


public class ApiServer extends io.dropwizard.Application<ApiConfiguration> {

    public static void main(String[] args) throws Exception {
        new ApiServer().run(args);
    }

    @Override
    public String getName() {
        return "energy";
    }

    @Override
    public void run(ApiConfiguration config, Environment environment) throws Exception {
        final JdbiFactory factory = new JdbiFactory();
        final Jdbi jdbi = factory.build(environment, config.getDataSourceFactory(), "postgresql");

        final DatabaseManagement databaseManagement = new DatabaseManagement(jdbi);
        final KafkaProducer kafkaProducer = createProducer();

        final CloseableManaged closeableManaged = new CloseableManaged(databaseManagement, kafkaProducer);
        environment.lifecycle().manage(closeableManaged);

        final DeviceDAO dao = jdbi.onDemand(DeviceDAO.class);
        environment.jersey().register(dao);

        final DeviceEndPoint deviceEndPoint = new DeviceEndPoint(kafkaProducer, config.getTopic(), dao, config.getDeviceTable());
        environment.jersey().register(deviceEndPoint);

    }

    private KafkaProducer createProducer(){
        Properties props = new Properties();
        // reasonable defaults
        props.put(ProducerConfig.ACKS_CONFIG, "1");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class.getName());
        props.put(SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8090");
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:29092");

        return new KafkaProducer<String, RawRecord>(props);
    }
}
