package fr.mycompany.m1.api;

import org.jdbi.v3.core.*;

import javax.inject.*;

@Singleton
public class DatabaseManagement {

    private final Jdbi jdbi;


    public DatabaseManagement(Jdbi jdbi) {
        this.jdbi = jdbi;
    }

    public void drop(){
        System.out.println("Create table !");
        jdbi.useTransaction((HandleConsumer<RuntimeException>) handle -> {
            handle.execute("DROP TABLE IF EXISTS devices");
        });
    }

    public void create() {
        System.out.println("Create table !");
        jdbi.useTransaction((HandleConsumer<RuntimeException>) handle -> {
            handle.execute("CREATE TABLE IF NOT EXISTS devices (\n" +
                    "  deviceId VARCHAR(64) PRIMARY KEY,\n" +
                    "  charging BOOLEAN NOT NULL\n" +
                    ");\n");
        });
    }
}
