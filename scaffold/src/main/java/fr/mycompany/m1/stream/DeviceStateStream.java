package fr.mycompany.m1.stream;

import fr.mycompany.common.*;
import fr.mycompany.m1.api.*;
import fr.mycompany.raw.m1.*;
import io.confluent.kafka.streams.serdes.avro.*;
import io.dropwizard.*;
import io.dropwizard.cli.*;
import io.dropwizard.jdbi3.*;
import io.dropwizard.setup.*;
import net.sourceforge.argparse4j.inf.*;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.serialization.*;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.*;
import org.jdbi.v3.core.*;
import org.slf4j.*;

import java.util.*;

import static io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG;

public class DeviceStateStream extends EnvironmentCommand<DeviceStateStreamConf> {

    private static Logger log = LoggerFactory.getLogger(DeviceStateStream.class);

    public DeviceStateStream(Application<DeviceStateStreamConf> application) {
        super(application,"stream", "process energy stream");
    }



    @Override
    protected void run(Environment environment, Namespace namespace, DeviceStateStreamConf config) throws Exception {
        final JdbiFactory factory = new JdbiFactory();
        final Jdbi jdbi = factory.build(environment, config.getDataSourceFactory(), "postgresql");

        final DatabaseManagement databaseManagement = new DatabaseManagement(jdbi);
        final DeviceDAO dao = jdbi.onDemand(DeviceDAO.class);

        new DeviceStateStream.DeviceStateStorageProcessor(config, dao, databaseManagement).buildStream();
    }

    public static void main(String[] args) throws Exception {
        log.info("Starting DeviceStateStream...");
        new App().run(args);
    }


    public static class DeviceStateStorageProcessor extends StreamProcessor {

        private Logger log = LoggerFactory.getLogger(DeviceStateStorageProcessor.class);

        private final DeviceDAO deviceDAO;
        private final DeviceStateStreamConf conf;

        private final DatabaseManagement databaseManagement;

        public DeviceStateStorageProcessor(DeviceStateStreamConf conf, DeviceDAO deviceDAO, DatabaseManagement databaseManagement) {
            this.deviceDAO = deviceDAO;
            this.conf = conf;
            this.databaseManagement =  databaseManagement;
        }


        void createDatabase() {
            databaseManagement.create();
        }

        private Properties configure(DeviceStateStreamConf conf){
            final Properties streamsConfiguration = new Properties();
            streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, conf.getApplicationId());
            streamsConfiguration.put(SCHEMA_REGISTRY_URL_CONFIG, conf.getKafka().get("schema-registry-url"));
            streamsConfiguration.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, conf.getKafka().get("bootstrap-servers"));
            streamsConfiguration.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
            streamsConfiguration.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, SpecificAvroSerde.class);
            return streamsConfiguration;
        }

        private Serde<RawRecord> initSerde(DeviceStateStreamConf conf) {
            Serde<RawRecord> valueSpecificAvroSerde = new SpecificAvroSerde<>();
            Map<String, String> serdeConfig = Collections.singletonMap(SCHEMA_REGISTRY_URL_CONFIG, conf.getKafka().get("schema-registry-url"));
            valueSpecificAvroSerde.configure(serdeConfig, false);
            return valueSpecificAvroSerde;
        }

        public void buildStream() throws InterruptedException {
            createDatabase();
            final Serde<RawRecord> valueSpecificAvroSerde = initSerde(conf);

            StreamsBuilder builder = new StreamsBuilder();
            builder.stream(conf.getSource(),
                            Consumed.with(Serdes.String(), valueSpecificAvroSerde))
                    .foreach((uuid, value) -> {
                        log.info("ingest message uuid: {}  value: {}", uuid, value);
                        long charging = value.getCharging();
                        deviceDAO.setDeviceState(conf.getDeviceTable(), uuid, charging > 0);
                    });
            final Topology topology = builder.build();

            KafkaStreams streams = new KafkaStreams(topology, configure(conf));

            streams.setUncaughtExceptionHandler((Thread thread, Throwable throwable) -> {
                log.error("Failed to consume stream !", throwable);
            });



            Runtime.getRuntime().addShutdownHook(new Thread(streams::close));

            log.info("Start stream processing...");
            streams.start();
        }

    }
}
