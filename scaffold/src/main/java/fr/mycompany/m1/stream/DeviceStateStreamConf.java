package fr.mycompany.m1.stream;

import com.fasterxml.jackson.annotation.*;
import fr.mycompany.common.*;
import io.dropwizard.db.*;

import javax.validation.*;
import javax.validation.constraints.*;
import java.util.*;

public class DeviceStateStreamConf extends StreamConfiguration {

    @NotNull
    private String applicationId;

    @NotNull
    private String source;

    @NotNull
    private String deviceTable;

    @Valid
    @NotNull
    private DataSourceFactory dataSourceFactory = new DataSourceFactory();

    @NotNull
    private Map<String, String> kafka;

    @JsonProperty("kafka")
    public Map<String, String> getKafka() {
        return kafka;
    }

    @JsonProperty("kafka")
    public void setKafka(Map<String, String> kafka) {
        this.kafka = kafka;
    }

    @JsonProperty("database")
    public void setDataSourceFactory(DataSourceFactory factory){
        this.dataSourceFactory = factory;
    }

    @JsonProperty("database")
    public DataSourceFactory getDataSourceFactory(){
        return this.dataSourceFactory;
    }

    @JsonProperty("deviceTable")
    public String getDeviceTable() {
        return deviceTable;
    }

    @JsonProperty("deviceTable")
    public void setDeviceTable(String deviceTable) {
        this.deviceTable = deviceTable;
    }

    @JsonProperty("source")
    public String getSource() {
        return source;
    }

    @JsonProperty("source")
    public void setSource(String source) {
        this.source = source;
    }

    @JsonProperty("applicationId")
    public String getApplicationId() {
        return applicationId;
    }

    @JsonProperty("applicationId")
    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

}
